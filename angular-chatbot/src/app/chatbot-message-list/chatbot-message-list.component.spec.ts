import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatbotMessageListComponent } from './chatbot-message-list.component';

describe('ChatbotMessageListComponent', () => {
  let component: ChatbotMessageListComponent;
  let fixture: ComponentFixture<ChatbotMessageListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChatbotMessageListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatbotMessageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
