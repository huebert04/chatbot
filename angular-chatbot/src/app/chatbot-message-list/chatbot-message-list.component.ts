import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Message } from '../models/Message';
import { ChatBotService} from '../ChatBotService';

@Component({
  selector: 'app-chatbot-message-list',
  templateUrl: './chatbot-message-list.component.html',
  styleUrls: ['./chatbot-message-list.component.css']
})
export class ChatbotMessageListComponent implements OnInit, AfterViewInit {
  messageList: Message[];

  @ViewChild('scrollframe', {static: false}) scrollFrame: ElementRef;
  @ViewChildren('item') itemElements: QueryList<any>;
  

  constructor(private chatBotService: ChatBotService) {

  }

  ngOnInit(): void {
    this.messageList = this.chatBotService.getArray();
  }

  private scrollContainer: any;
  private items = [];

  ngAfterViewInit() {
    this.scrollContainer = this.scrollFrame.nativeElement;  
    this.itemElements.changes.subscribe(_ => this.onItemElementsChanged());    

    // Add a new item every 2 seconds
    setInterval(() => {
      this.items.push({});
    }, 2000);
  }
  
  private onItemElementsChanged(): void {
    this.scrollToBottom();
  }

  private scrollToBottom(): void {
    this.scrollContainer.scroll({
      top: this.scrollContainer.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
  }

}
