import { Component, OnInit} from '@angular/core';
import { ChatBotService } from '../ChatBotService'
import { Message } from '../models/Message';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css']
})
export class ChatbotComponent implements OnInit {
  toggled = true
  toggleArrow = "V"
  userMsg: Message
  botMsg: Message
  inputtext:string = "";
  

  constructor(private chatBotService: ChatBotService) { }

  ngOnInit(): void {
    this.getAnswer("Hello!");
  }

  toggleBox(){
    this.toggled = !this.toggled;
    if(this.toggleArrow == "V")
      this.toggleArrow = "/\\"
    else
      this.toggleArrow = "V"
  }

  getAnswer(event) {
    this.userMsg = new Message();
    this.userMsg.user = false;
    console.log(event);
    this.userMsg.message = event;
    this.chatBotService.push(this.userMsg);

    this.chatBotService.getAnswer(event).subscribe(results => {
      this.botMsg = new Message();
      this.botMsg.user = true;
      this.botMsg.message = results;
      this.chatBotService.push(this.botMsg);
    });
    
    //Clear input field
    this.inputtext = "";
  }

}
