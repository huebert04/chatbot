import {Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ChatBotAnswer} from "./interface";
import { Message } from './models/Message';

@Injectable({
    providedIn: "root"
})
export class ChatBotService {
    chatbotAPI: any;
    private messages: Message[]

    constructor(private http: HttpClient){
        this.chatbotAPI = "https://localhost:44322";
        this.messages = [];
    }

    getAnswer(question : string) {
        return this.http.get(`${this.chatbotAPI}/api/chatbot?question="${question}"`, {responseType:"text"});
    }

    push(msg: Message) {
        this.messages.push(msg);
    }

    getArray() {
        return this.messages;
    }
    
}

