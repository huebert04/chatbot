import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../models/Message'

@Component({
  selector: 'app-chatbot-message',
  templateUrl: './chatbot-message.component.html',
  styleUrls: ['./chatbot-message.component.css']
})
export class ChatbotMessageComponent implements OnInit {
  @Input() msg: Message;
  constructor() { }

  ngOnInit(): void {

  }

}
