using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ChatBot;
using ChatBot.Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Hosting;

namespace ChatBot.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatBotController : ControllerBase
    {
        private readonly ChatBotContext _context;

        public ChatBotController(ChatBotContext context)
        {
            _context = context;
        }

       
        [HttpGet]
        public async Task<string> GetObjects(string question)
        {
            var finder = new KeyWordFinder(_context);
            (string, int) solution = (null, -1);

            var thing = finder.GetMatchFromUserInput(question);
            
            for(int i = 0; i < thing.Count; i++)
            {
                if((solution.Item1 == null && solution.Item2 == -1) || thing[i].Item2 < solution.Item2)
                {
                    solution = thing[i];
                }
            }

            // Hvis laveste LD er for h?y, blir det default istede
            if (solution.Item2 >= 15)
                solution.Item1 = "unknown";

            Console.WriteLine(solution);
            Console.WriteLine(LevenshteinDistance.Calculate("l?nn","hvordan er l?nn?"));

            return Respond(solution.Item1);
            //return obj;
        }

        public string Respond(string keyword)
        {
            var resp = new Responder(_context);
            var ret = resp.Respond(keyword);
            return ret;
        }

        // GET: api/ChatBot/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ChatBotObject>> GetChatBotObject(long id)
        {
            var chatBotObject = await _context.Objects.FindAsync(id);

            if (chatBotObject == null)
            {
                return NotFound();
            }

            return chatBotObject;
        }

        // PUT: api/ChatBot/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChatBotObject(long id, ChatBotObject chatBotObject)
        {
            if (id != chatBotObject.Id)
            {
                return BadRequest();
            }

            _context.Entry(chatBotObject).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChatBotObjectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ChatBot
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<ActionResult<ChatBotObject>> PostChatBotObject(ChatBotObject chatBotObject)
        {
            _context.Objects.Add(chatBotObject);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetChatBotObject", new { id = chatBotObject.Id }, chatBotObject);
        }

        // DELETE: api/ChatBot/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ChatBotObject>> DeleteChatBotObject(long id)
        {
            var chatBotObject = await _context.Objects.FindAsync(id);
            if (chatBotObject == null)
            {
                return NotFound();
            }

            _context.Objects.Remove(chatBotObject);
            await _context.SaveChangesAsync();

            return chatBotObject;
        }

        private bool ChatBotObjectExists(long id)
        {
            return _context.Objects.Any(e => e.Id == id);
        }
    }
}
