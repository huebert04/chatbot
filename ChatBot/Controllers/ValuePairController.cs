﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ChatBot;
using ChatBot.Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Hosting;


namespace ChatBot.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuePairController : ControllerBase
    {
        private readonly ChatBotContext _context;

        public ValuePairController(ChatBotContext context)
        {
            _context = context;
        }

        //Get all 
        [HttpGet]
        public async Task<IActionResult> GetList()
        {
            
            var keypairs = await _context.ValuePairs.ToListAsync();
            var output = keypairs.Select(e => new
            {
                e.Keyword,
                e.Phrase,
            });

            return Ok(output);
        }

        //Post
        [HttpPost]
        public async Task<IActionResult> New(ValuePair valuePair)
        {
            _context.ValuePairs.Add(valuePair);
            await _context.SaveChangesAsync();
            return Ok(valuePair);
        }


        //Delete
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var entity = _context.ValuePairs
                                .Where(e => e.Id == id)
                                .FirstOrDefault();

            if (entity == null)
                return NotFound();

            this._context.ValuePairs.Remove(entity);
            this._context.SaveChanges();

            return NoContent();
        }
    }
}
