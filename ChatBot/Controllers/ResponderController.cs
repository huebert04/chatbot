﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ChatBot;
using ChatBot.Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Hosting;

namespace ChatBot.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResponderController: ControllerBase
    {
        private readonly ChatBotContext _context;

        public ResponderController(ChatBotContext context)
        {
            _context = context;
        }

        /*public void UpdateDb()
        {
            using (_context)
            {
                _context.Database.EnsureDeleted();
                _context.Database.EnsureCreated();
                var responses = new List<Response>
                {
                    new Response{Keyword="greeting", Phrase="Hello there General Kenobi!"},
                    new Response{Keyword="greeting", Phrase="Hei på deg!"},
                    new Response{Keyword="greeting", Phrase="Wazzap!"},
                    new Response{Keyword="money", Phrase="Programmet koster ikke noe, og du vil få lønn under hele programmet."},
                    new Response{Keyword="money", Phrase="Det koster ingenting, og du vil få lønn under hele programmet."},
                    new Response{Keyword="money", Phrase="Som ansatt i Experis vil du få lønn under hele programmet og vi betaler for Accelerated learning perioden."},
                    new Response{Keyword="tenniure", Phrase="Stillingen er en fulltidstilling."},
                    new Response{Keyword="tenniure", Phrase="I Experis vil du ha en fulltidsjobb hvor du i starten vil få opplæring og senere jobbe i en bedrift."},
                    new Response{Keyword="education", Phrase="Vi har krav om minimum en bachelorgrad. Dersom utdanningen har inneholdt koding (eks. innen fysikk, robotikk) kan du være aktuell og vi anbefaler å legge inn en søknad!"},
                    new Response{Keyword="education", Phrase="Det er et krav om en Bachelorgrad, denne trenger ikke være innen data så lenge utdanningen har inneholdt koding (eks. fysikk, robotikk). dersom studieløpet har inneholdt koding anbefaler vi å legge inn en søknad."},
                    new Response{Keyword="language-us", Phrase="Yes, you need to know Norwegian."},
                    new Response{Keyword="language", Phrase="Ja du må kunne snakke norsk."},
                    new Response{Keyword="duration", Phrase="12-15 måneder."},
                    new Response{Keyword="unknown", Phrase="Beklager, men jeg tror ikke jeg forstod helt hva du sa."},
                    new Response{Keyword="unknown", Phrase="Nani?!"}
                };

                responses.ForEach(r => _context.Responses.Add(r));
                _context.SaveChanges();


            }
        }*/

        [HttpGet]
        public async Task<IActionResult> GetObjects()
        {
            
            var responses = await _context.Responses.ToListAsync();
            var output = responses.Select(e => new
            {
                e.Keyword,
                e.Phrase,
            });

            return Ok(output);
        }

        //post
        [HttpPost]
        public async Task<IActionResult> New(Response response)
        {
            _context.Responses.Add(response);
            await _context.SaveChangesAsync();
            return Ok(response);
        }

        //Delete
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var entity = _context.Responses
                                .Where(e => e.Id == id)
                                .FirstOrDefault();

            if (entity == null)
                return NotFound();

            this._context.Responses.Remove(entity);
            this._context.SaveChanges();

            return NoContent();
        }


    }
}
