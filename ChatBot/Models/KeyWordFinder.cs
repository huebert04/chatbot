﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Threading.Tasks;

namespace ChatBot.Models
{
    public class KeyWordFinder
    {
        private readonly ChatBotContext _context;

        public KeyWordFinder(ChatBotContext context)
        {
            _context = context;
        }

        //List<(string, string)> ValuePair;

        public IList<(string, int)> GetMatchFromUserInput(string str)
        {
            str.ToLower();
            var resp = GetLevensteinDistanceToValuePairs(str);

            return resp;
        }

        private List<(string, int)> GetLevensteinDistanceToValuePairs(string str)
        {
            var result = _context.ValuePairs.ToList();

            var resp = new List<(string, int)>();

            foreach (var value in result)
            {
                resp.Add((value.Keyword, LevenshteinDistance.Calculate(value.Phrase, str)));
            }
            return resp;
        }

        /*public void BuildKeyValuePair()
        {
            ValuePair = new List<(string, string)> 
            {
                ("greeting", "hei" ),
                ("greeting","halo"),
                ("money", "hvor mye får man betalt?"),
                ("money","får man betalt?"),
                ("money","hvor mye får man?"),
                ("money","Lønn"),
                ("money","Lønning"),
                ("tenniure", "stillingsprosent"),
                ("tenniure","hvor mange timer i uka er det?"),
                ("tenniure", "hvor lenge er man bundet?"),
                ("education","må man ha datautdaning?"),
                ("education", "må man ha en bachelor?"),
                ("education","hva kreves for denne stillingen?"),
                ("language-us","do you need to know norwegian?"),
                ("language", "må man kunne snakke norsk?"),
                ("duration","hvor lenge er man bundet?"),
                ("duration", "hvor lenge varer programmet?")
            };
        }*/
    }
}
