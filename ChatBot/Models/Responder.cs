﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ChatBot.Models
{
    public class Responder
    {
        private readonly ChatBotContext _context;

        public Responder(ChatBotContext context)
        {
            _context = context;
        }

        public string Respond(string keyValue)
        {
            var possibleResponses = GetAvailableResponses();
            var listResp = possibleResponses[keyValue];
            Random r = new Random();
            int rInt = r.Next(0, listResp.Count);
            return listResp[rInt];
        }

        public Dictionary<string, List<string>> GetAvailableResponses()
        {
            var result = _context.Responses.ToList();

            var dictionary = new Dictionary<string, List<string>>();

            foreach(var value in result)
            {
                if(!dictionary.ContainsKey(value.Keyword) || dictionary.Count == 0)
                {
                    var phrases = new List<string>
                        {
                            value.Phrase
                        };

                    dictionary.Add(value.Keyword, phrases);
                } else
                {
                    dictionary[value.Keyword].Add(value.Phrase);
                }
            }
           
            return dictionary;
            
            /*return new Dictionary<string, List<string>>()
            {
                { "greeting",new List<string>{
                    "Hello there General Kenobi",
                    "Hei på deg",
                    "Wazzap!" } 
                },
                { "money",new List<string>{
                    "Programmet koster ikke noe, og du vil få lønn under hele programmet",
                    "Det koster ingenting, og du vil få lønn under hele programmet",
                    "Som ansatt i Experis vil du få lønn under hele programmet og vi betaler for Accelerated learning perioden"
                    } 
                },
                { "tenniure",new List<string>{
                    "Stillingen er en fulltidstilling",
                    "I Experis vil du ha en fulltidsjobb hvor du i starten vil få opplæring og senere jobbe i en bedrift"
                    } 
                },
                {"education", new List<string>{
                    "Vi har krav om minimum en bachelorgrad. Dersom utdanningen har inneholdt koding (eks. innen fysikk, robotikk) kan du være aktuell og vi anbefaler å legge inn en søknad!",
                    "Det er et krav om en Bachelorgrad, denne trenger ikke være innen data så lenge utdanningen har inneholdt koding (eks. fysikk, robotikk). dersom studieløpet har inneholdt koding anbefaler vi å legge inn en søknad"
                    }
                },
                { "unknown",new List<string>{ 
                    "Beklager, men jeg tror ikke jeg forstod helt hva du sa.",
                    "Nani?!"
                } }
            };*/
        }
    }
}
