﻿using System.ComponentModel.DataAnnotations;
namespace ChatBot.Models
{
    public class ValuePair
    {
        [Key]
        public long Id { get; set; }

        public string Keyword { get; set; }

        public string Phrase { get; set; }
        
    }
}
