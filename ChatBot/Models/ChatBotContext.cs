﻿using Microsoft.EntityFrameworkCore;

namespace ChatBot.Models
{
    public class ChatBotContext : DbContext
    {
        public ChatBotContext(DbContextOptions<ChatBotContext> options) : base(options)
        {

        }
        public DbSet<ChatBotObject> Objects { get; set; }
        public DbSet<ValuePair> ValuePairs { get; set; }
        public DbSet<Response> Responses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=./chatbot.sqlite");
            //base.OnConfiguring(optionsBuilder);
        }

    }
}
