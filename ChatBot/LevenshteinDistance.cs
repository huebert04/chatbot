﻿using System;

namespace ChatBot
{
    public static class LevenshteinDistance
    {
        public static int Calculate(string str1, string str2)
        {
            var Str1Length = str1.Length;
            var Str2Length = str2.Length;

            var matrix = new int[Str1Length + 1, Str2Length + 1];

            //First Calculation
            if (Str1Length == 0)
            {
                return Str2Length;
            }

            if (Str2Length == 0)
            {
                return Str1Length;
            }

            for (var i = 0; i <= Str1Length; matrix[i, 0] = i++) { }

            for (var j = 0; j <= Str2Length; matrix[0, j] = j++) { }

            for (var i = 1; i <= Str1Length; i++)
            {
                for (var j = 1; j <= Str2Length; j++)
                {
                    var cost = (str2[j - 1] == str1[i - 1]) ? 0 : 1;

                    matrix[i, j] = Math.Min(Math.Min(matrix[i - 1, j] + 1, matrix[i, j - 1] + 1), matrix[i - 1, j - 1] + cost);
                }
            }

            return matrix[Str1Length, Str2Length];

        }
    }
}
